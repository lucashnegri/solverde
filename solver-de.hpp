#define BOOST_DISABLE_ASSERTS
#include <boost/multi_array.hpp>
#include <random>
#include <tuple>

#ifndef __clang__
#include <omp.h>
#endif

namespace SolverDE
{
    
typedef boost::multi_array<double, 2> Array2D;
typedef Array2D::const_reference ConstRef;
typedef boost::multi_array<double, 1> Array1D;

template <typename F>
struct Solver
{
    Solver(int n_pop_, int n_dim_, int seed = -1):
        pop(boost::extents[n_pop_][n_dim_]), trial(boost::extents[n_pop_][n_dim_]),
        fit(boost::extents[n_pop_]), trial_fit(boost::extents[n_pop_]),
        limits(boost::extents[n_dim_][2]), f(0.7), cr(0.95),
        n_pop(n_pop_), n_dim(n_dim_)
    {
        if(seed == -1)
        {
            std::random_device rd;   
            seed = rd();
        }

        rng = std::mt19937(seed);

        for(int i = 0; i < n_dim; ++i)
        {
            limits[i][0] = -1e99;
            limits[i][1] =  1e99;
        }
    }

    void init()
    {
        for(auto cand : pop)
            for(int d = 0; d < n_dim; ++d)
                cand[d] = rand(limits[d][0], limits[d][1]);

        evaluate(fit, pop);
    }
        
    inline void step()
    {
        gen_trial();                // generation of the trial vectors
        evaluate(trial_fit, trial); // evaluation of the trial vectors
        selection();                // selection
    }

    int run(int n_it, int n_stuck)
    {
        init();
        double best_fit = -1e99;
        int stuck = 0;

        int it;

        for(it = 0; it < n_it; ++it)
        {
            step();
            const int best = besti();

            if(fit[best] / best_fit <= 0.9999) // at least a 0,001% enhancement
            {
                best_fit = fit[best];
                stuck    = 0;
            }
            else
                ++stuck;

            if(stuck > n_stuck) break;
        }

        return it;
    }

    void gen_trial()
    {
        for(int p = 0; p < n_pop; ++p)
        {
            int r0, r1, r2;

            do r0 = rand(n_pop); while(r0 == p);
            do r1 = rand(n_pop); while(r1 == r0);
            do r2 = rand(n_pop); while(r2 == r1);

            const int j = rand(n_dim);

            for(int d = 0; d < n_dim; ++d)
            {
                if(rand(0.0, 1.0) < cr || d == j)
                    trial[p][d] = pop[r0][d] + f * (pop[r1][d] - pop[r2][d]);
                else
                    trial[p][d] = pop[p][d];
                
                trial[p][d] = std::max(limits[d][0], std::min(limits[d][1], trial[p][d]));
            }
        }
    }

    void evaluate(Array1D& store, const Array2D& pop)
    {
        #pragma omp parallel for
        for(int p = 0; p < n_pop; ++p)
            store[p] = fitness(pop[p]);
    }

    void selection()
    {
        for(int p = 0; p < n_pop; ++p)
        {
            if(trial_fit[p] >= fit[p])
            {
                fit[p] = trial_fit[p];
                pop[p] = trial[p];
            }
        }
    }

    int besti() const
    {
        int best = 0;

        for(int i = 1; i < n_pop; ++i)
            if(fit[i] > fit[best])
                best = i;

        return best;
    }
    
    std::tuple<double, Array1D> best() const
    {
        const int i = besti();
        return std::make_tuple(fit[i], pop[i]);
    }
    
    inline int rand(int i)
    {
        return rng() % i;
    }

    inline double rand(double a, double b)
    {
        return (rng() / (double)rng.max()) * (b - a) + a;
    }
    
    /* member data. public */
    
    Array2D pop, trial;
    Array1D fit, trial_fit;
    Array2D limits;
    
    std::mt19937 rng;
    F fitness;

    double  f;    
    double  cr;
    
    int n_pop, n_dim;
};
}
