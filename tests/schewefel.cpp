#include "../solver-de.hpp"
#include <iostream>
#include <cmath>

using namespace SolverDE;
using namespace std;

// functor to compute fitness
struct SchewefelFitness
{
    double operator()(ConstRef cand) const
    {
        double sum = 0.0;
    
        for(double v : cand)
            sum += v * std::sin(std::sqrt(std::abs(v)));
        
        return sum;
    }
};

int main()
{
    const int pop = 50, dim = 10;
    
    random_device rd;
    Solver<SchewefelFitness > solver(pop, dim, rd() );
    
    for(int i = 0; i < dim; ++i)
    {
        solver.limits[i][0] = -500.0;
        solver.limits[i][1] =  500.0;
    }
        
    solver.cr = 0.5;
    solver.f  = 0.8;
    
    int it    = solver.run(10000, 1000);
    auto best = solver.best();
    
    cout << "Iterations: " << it  << endl;
    cout << "Fitness   : " << std::get<0>(best) << " (should be " << (418.9829 * dim) << ")" << endl;
    cout << "Solution  :";
    
    for(double v : std::get<1>(best))
        cout << " " << v;
    cout << endl;
    
    return 0;
}
