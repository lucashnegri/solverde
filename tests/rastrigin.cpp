#include "../solver-de.hpp"
#include <random>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <tuple>

using namespace SolverDE;
using namespace std;

struct RastriginFitness
{
    double operator()(ConstRef cand) const
    {
        const double x = cand[0], y = cand[1];
        const double a = x*x - 10 * cos(2 * M_PI * x);
        const double b = y*y - 10 * cos(2 * M_PI * y);
        return -(20 + a + b);
    }
};

int main()
{
    random_device rd;
    int n_pop = 20, n_dim = 2;
    
    Solver<RastriginFitness> solver(n_pop, n_dim, rd() );
    
    for(int i = 0; i < 2; ++i)
    {
        solver.limits[i][0] = -5.12;
        solver.limits[i][1] =  5.12;
    }
    
    solver.cr = 0.9;
    solver.f  = 0.8;
    
    int it = solver.run(500, 50);
    auto best = solver.best();
    
    cout << "Iterations: " << it  << endl;
    cout << fixed << setprecision(2);
    
    cout << "Fitness   : " << -std::get<0>(best) << ", expected: " << 0.0 << endl;
    cout << "Solution  :";
    
    for(double v : std::get<1>(best))
        cout << " " << v;
    
    cout << endl;
    
    return 0;
}
