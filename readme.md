# SolverDE

SolverDE is a simple and fast header-only implementation of the differential
evolution algorithm in C++11. Can use OpenMP to parallelize fitness evaluations.

## Usage

Check the examples on _tests/_. For more information, see _solver-de.hpp_.
The fitness evaluation function should be implemented as a _functor_, that defines
_operator(cand)_, where cand is an double array (boost multiarray) representing
an individual.

## License

solver-de is released under the MIT license.

## Compiling the tests

Example (gcc): g++ -Wall -std=c++11 -fopenmp -O2 tests/rastrigin.cpp -o rastrigin

## Contact

Lucas Hermann Negri - lucashnegri@gmail.com
http://oproj.tuxfamily.org
